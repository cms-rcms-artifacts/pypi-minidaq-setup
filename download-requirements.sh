#!/usr/bin/env bash

TARGET_PYTHON_VERSION=3.6

rm -rf ./requirements

/usr/bin/env pip download --python-version "${TARGET_PYTHON_VERSION}" --only-binary :all: -r requirements.txt -d ./requirements
