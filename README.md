# DAQ3 MiniDAQ Setup Generator Python requirements

This repository contains the Python packages required by the minidaq3-setup-generator found at: https://gitlab.cern.ch/cms-rcms/cdaq/minidaq3-setup-generator

## Updating packages

The `requirements.txt` file in this repository needs to be the same as in the minidaq3-setup-generator repository.

Executing `bash download-requirements.sh` in this repository's root directory will update the packages.

After committing and pushing the changes using Git, the updated packages will be available at P5.
